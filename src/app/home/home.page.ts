import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  ngOnInit(): void {
    this.presentToast("ngOnInit was executed", 5000, "middle");
  }

  constructor(public toastController: ToastController) {}

  async presentToast(m: string, ms: number, p: string) {
    const toast = await this.toastController.create({
      message: m,
      duration: ms,
      position: p
    });
    toast.present();
  }

  showToast() {
    this.presentToast("Button was clicked", 3000, "top");
    this.presentToast("Button was clicked here too", 3000, "bottom");
  }

}
